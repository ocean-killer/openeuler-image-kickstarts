# Kickstart file for openEuler RISC-V (riscv64) Nano 20.03

#repo --name="koji-override-0" --baseurl=http://repo.openeuler.org/

install
text
#reboot
lang en_US.UTF-8
keyboard us
# short hostname still allows DHCP to assign domain name
network --bootproto dhcp --device=link --hostname=openeuler-riscv
rootpw riscv
firewall --enabled --ssh
timezone --utc Asia/Shanghai
selinux --disabled
services --enabled=sshd,NetworkManager,chronyd

bootloader --location=none --disabled

zerombr
clearpart --all --initlabel --disklabel=gpt
part swap --asprimary --label=swap --size=32
part /boot/efi --fstype=vfat --size=128
part /boot --size=512 --fstype ext4 --asprimary
part / --fstype="ext4" --size=8192

# Halt the system once configuration has finished.
poweroff

%packages --excludedocs --ignoremissing
@core
NetworkManager
dracut-config-rescue
iproute
iputils
parted
plymouth
policycoreutils
procps-ng
sudo
dnf-plugins-core
rootfiles
firewalld
selinux-policy-targeted
yum
tar

kpartx
pigz

kernel
kernel-devel
linux-firmware

# JH7100 specific packages
jh7100-firmware
brcm-patchram-plus

bash
dnf

openssh

glibc-static

lsof
nano

chrony

systemd-udev
vim-minimal
bluez

hostname
htop
tmux
wget

passwd
coreutils
util-linux

%end

%post
# Disable default repositories (not riscv64 in upstream)
# dnf config-manager --set-disabled rawhide updates updates-testing openEuler openEuler-modular openEuler-cisco-openh264 updates-modular updates-testing-modular

# TODO: BCMDHD driver is too unstable to enable will removed after driver debug
cat > /etc/modprobe.d/bcmdhd-blacklist.conf << EOF
blacklist bcmdhd
EOF

# TODO: make sure the dracut add dw_mmc-pltfm but remove bcmdhd in initrd
cat > /etc/dracut.conf.d/jh7100.conf << EOF
add_drivers+="dw_mmc-pltfm"
omit_drivers+="bcmdhd"
EOF

echo 'nameserver 114.114.114.114' >> /etc/resolv.conf

pushd /tmp

/usr/bin/wget -P boot "http://openkoji.iscas.ac.cn/pub/dl/riscv/StarFive/JH7100/kernel-cross/oe/kernel-cur.tar"
/usr/bin/wget -P boot "http://openkoji.iscas.ac.cn/pub/dl/riscv/riscv64/grubriscv64.efi"

pushd boot
/usr/bin/tar xpf kernel-cur.tar
rm -vf kernel-cur.tar
popd

mv -vf boot/lib/modules/* /lib/modules/
rm -rf boot/lib
mkdir -p /boot/efi/EFI/openEuler
mv -vf boot/grubriscv64.efi /boot/efi/EFI/openEuler/
mv -vf boot/* /boot

rm -r boot

popd

CROSS_KVER=`cat /boot/version.txt`
ROOT_UUID=`grep ' / ' /etc/fstab | awk '{printf $1}' | sed -e 's/^UUID=//g'`

sed -i -e "s/@@ROOTUUID@@/$ROOT_UUID/g"   \
    -e "s/@@KVERSION@@/${CROSS_KVER}/g" /boot/grub.cfg

# Auto download bluetooth firmware on startup
cat << EOF >> /etc/rc.d/rc.local

# reset Bluetooth module
gpioset 0 35=0; sleep 1; gpioset 0 35=1

# download proper firmware depends on chip module
if [ `dmesg | grep -c 'BCM43430/1'` -gt 0 ]; then
    brcm_patchram_plus --enable_hci --no2bytes --tosleep 200000 --baudrate 115200 --patchram /lib/firmware/brcm/BCM43430A1.hcd /dev/ttyS1 &
fi

if [ `dmesg | grep -c 'BCM43430/2'` -gt 0 ]; then
    brcm_patchram_plus --enable_hci --no2bytes --tosleep 200000 --baudrate 115200 --patchram /lib/firmware/brcm/BCM4343B0.hcd /dev/ttyS1 &
fi

EOF

chmod +x /etc/rc.d/rc.local

# Create uEnv.txt for u-boot
mkdir -p /boot/boot
cat << EOF > /boot/boot/uEnv.txt
fdt_high=0xffffffffffffffff
initrd_high=0xffffffffffffffff

scriptaddr=0x88100000
script_offset_f=0x1fff000
script_size_f=0x1000

kernel_addr_r=0x84000000
kernel_comp_addr_r=0x90000000
kernel_comp_size=0x10000000

fdt_addr_r=0x88000000
ramdisk_addr_r=0x88300000

bootcmd=load mmc 0:2 0xa0000000 /EFI/openEuler/grubriscv64.efi; bootefi 0xa0000000
bootcmd_mmc0=devnum=0; run mmc_boot

ipaddr=192.168.120.200
netmask=255.255.255.0
EOF


# Create openEuler RISC-V repo
cat << EOF > /etc/yum.repos.d/openEuler-riscv.repo
[openEuler-riscv-oepkgs]
name=openEuler RISC-V on oepkgs
baseurl=https://repo.oepkgs.net/oepkgs-oe-riscv/combine-appliance/\$arch/
enabled=1
gpgcheck=0

[openEuler-riscv-oepkgs-noarch]
name=openEuler RISC-V on oepkgs - noarch
baseurl=https://repo.oepkgs.net/oepkgs-oe-riscv/combine-appliance/noarch/
enabled=1
gpgcheck=0

EOF

# systemd starts serial consoles on /dev/ttyS0 and /dev/hvc0.  The
# only problem is they are the same serial console.  Mask one.
systemctl mask serial-getty@hvc0.service

# setup login message
cat << EOF | tee /etc/issue /etc/issue.net
Welcome to the openEuler/RISC-V disk image

Build date: $(date --utc)

Kernel \r on an \m (\l)

The root password is ‘riscv’.

To install new packages use 'dnf install ...'

To upgrade disk image use 'dnf upgrade --best'

If DNS isn’t working, try editing ‘/etc/yum.repos.d/openEuler-riscv.repo’.

EOF
%end

%post --nochroot

/usr/sbin/sfdisk -l /dev/loop0

echo 'Deleting the first partition of Image...'
/usr/sbin/sfdisk --delete /dev/loop0 1
/usr/sbin/sfdisk -l /dev/loop0

echo 'Reorder the partitions of Image...'
#/usr/sbin/sfdisk -fr /dev/loop0
/usr/sbin/sfdisk -l /dev/loop0

# Confirm boot partion's label is 'boot'
/usr/sbin/e2lable /dev/loop03 boot

popd

%end

# EOF
