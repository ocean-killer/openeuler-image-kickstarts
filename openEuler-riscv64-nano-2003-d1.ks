# Kickstart file for openEuler RISC-V (riscv64) Nano 20.03

#repo --name="koji-override-0" --baseurl=http://repo.openeuler.org/

install
text
#reboot
lang en_US.UTF-8
keyboard us
# short hostname still allows DHCP to assign domain name
network --bootproto dhcp --device=link --hostname=openeuler-riscv
rootpw riscv
firewall --enabled --ssh
timezone --utc Asia/Shanghai
selinux --disabled
services --enabled=sshd,NetworkManager,chronyd

bootloader --location=none --disabled

zerombr
clearpart --all --initlabel --disklabel=msdos
part swap --asprimary --label=swap --size=32
part /boot/efi --fstype=vfat --size=128
part /boot --size=512 --fstype ext4 --asprimary
part / --fstype="ext4" --size=8192

# Halt the system once configuration has finished.
poweroff

%packages --excludedocs --ignoremissing
@core
NetworkManager
dracut-config-rescue
iproute
iputils
parted
plymouth
policycoreutils
procps-ng
sudo
dnf-plugins-core
rootfiles
firewalld
selinux-policy-targeted
yum
tar

kpartx
pigz

kernel
kernel-devel
linux-firmware

bash
dnf

openssh

glibc-static

lsof
nano

chrony

systemd-udev
vim-minimal
bluez

hostname
htop
tmux
wget

passwd
coreutils
util-linux

%end

%post

echo 'nameserver 114.114.114.114' >> /etc/resolv.conf

pushd /tmp

/usr/bin/wget -P boot "http://openkoji.iscas.ac.cn/pub/dl/riscv/Allwinner/Nezha_D1/kernel-cross/oe/kernel-cur.tar"
/usr/bin/wget -P boot "http://openkoji.iscas.ac.cn/pub/dl/riscv/Allwinner/Nezha_D1/kernel-cross/oe/extlinux.conf"

pushd boot
/usr/bin/tar  xpf kernel-cur.tar
popd

mkdir -p /boot/extlinux
mv -vf boot/extlinux.conf 

mv -vf boot/lib/modules/* /lib/modules/
mv -vf boot/boot-5.4.img /boot
mv -vf boot/*.dtb /boot
mv -vf boot/uEnv.txt /boot
mv -vf boot/extlinux.conf /boot/extlinux/extlinux.conf
mv -vf boot/*-5.4.61* /boot

rm -r boot

popd

%end

%post
# Disable default repositories (not riscv64 in upstream)
# dnf config-manager --set-disabled rawhide updates updates-testing openEuler openEuler-modular openEuler-cisco-openh264 updates-modular updates-testing-modular

sed -i 's|noatime|noatime,x-systemd.device-timeout=300s,x-systemd.mount-timeout=300s|g' /etc/fstab

# remove unnecessary entry in /etc/fstab

sed -i '/swap/d' /etc/fstab
sed -i '/efi/d' /etc/fstab

ROOT_UUID=`grep ' / ' /etc/fstab | awk '{printf $1}' | sed -e 's/^UUID=//g'`

sed -i -e "s/@@ROOTUUID@@/$ROOT_UUID/g"   \
    -e "s/@@BUILD_RELEASE@@/$(date +%Y%m%d-%H%M%S)/g" /boot/extlinux/extlinux.conf

# Create openEuler RISC-V repo
cat << EOF > /etc/yum.repos.d/openEuler-riscv.repo
[openEuler-riscv-oepkgs]
name=openEuler RISC-V on oepkgs
baseurl=https://repo.oepkgs.net/oepkgs-oe-riscv/combine-appliance/$arch/
enabled=1
gpgcheck=0

[openEuler-riscv-oepkgs-noarch]
name=openEuler RISC-V on oepkgs - noarch
baseurl=https://repo.oepkgs.net/oepkgs-oe-riscv/combine-appliance/noarch/
enabled=1
gpgcheck=0

EOF

# systemd starts serial consoles on /dev/ttyS0 and /dev/hvc0.  The
# only problem is they are the same serial console.  Mask one.
systemctl mask serial-getty@hvc0.service

# setup login message
cat << EOF | tee /etc/issue /etc/issue.net
Welcome to the openEuler/RISC-V disk image

Build date: $(date --utc)

Kernel \r on an \m (\l)

The root password is ‘riscv’.

To install new packages use 'dnf install ...'

To upgrade disk image use 'dnf upgrade --best'

If DNS isn’t working, try editing ‘/etc/yum.repos.d/openEuler-riscv.repo’.

EOF
%end

%post --nochroot

/usr/sbin/sfdisk -l /dev/loop0

echo 'Deleting the first partition of Image...'
/usr/sbin/sfdisk --delete /dev/loop0 1
/usr/sbin/sfdisk -l /dev/loop0

echo 'Reorder the partitions of Image...'
/usr/sbin/sfdisk -r /dev/loop0
/usr/sbin/sfdisk -l /dev/loop0

pushd /tmp
/usr/bin/wget -P boot "http://openkoji.iscas.ac.cn/pub/dl/riscv/Allwinner/Nezha_D1/FW/boot0_sdcard_sun20iw1p1.bin"
/usr/bin/wget -P boot "http://openkoji.iscas.ac.cn/pub/dl/riscv/Allwinner/Nezha_D1/FW/boot_package.fex"

dd if=boot/boot0_sdcard_sun20iw1p1.bin of=/dev/loop0 seek=16 bs=512
dd if=boot/boot_package.fex of=/dev/loop0 seek=32800 bs=512

popd

%end

# EOF
