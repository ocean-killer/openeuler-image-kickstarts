# Kickstart file for openEuler RISC-V (riscv64) Nano 20.03

#repo --name="koji-override-0" --baseurl=http://repo.openeuler.org/

install
text
#reboot
lang en_US.UTF-8
keyboard us
# short hostname still allows DHCP to assign domain name
network --bootproto dhcp --device=link --hostname=openeuler-riscv
rootpw riscv
firewall --enabled --ssh
timezone --utc Asia/Shanghai
selinux --disabled
services --enabled=sshd,NetworkManager,chronyd

bootloader --location=none --disabled

zerombr
clearpart --all --initlabel --disklabel=gpt
part /boot/efi --fstype=vfat --size=128
part /boot --size=512 --fstype ext4 --asprimary
part / --fstype="ext4" --size=8192

# Halt the system once configuration has finished.
poweroff

%packages --excludedocs --ignoremissing

-dracut
-dracut-config-rescue
-libkcapi-hmaccalc
-plymouth

kernel
kernel-devel
linux-firmware

bash
dnf

openssh

glibc-static

lsof
nano

chrony

systemd-udev
vim-minimal

hostname
htop
tmux

passwd
coreutils
util-linux

%end

%post
# Disable default repositories (not riscv64 in upstream)
# dnf config-manager --set-disabled rawhide updates updates-testing openEuler openEuler-modular openEuler-cisco-openh264 updates-modular updates-testing-modular

# Create openEuler RISC-V repo
cat << EOF > /etc/yum.repos.d/openEuler-riscv.repo
[openEuler-riscv-oepkgs]
name=openEuler RISC-V on oepkgs
baseurl=https://repo.oepkgs.net/oepkgs-oe-riscv/combine-appliance/$arch/
enabled=1
gpgcheck=0

[openEuler-riscv-oepkgs-noarch]
name=openEuler RISC-V on oepkgs - noarch
baseurl=https://repo.oepkgs.net/oepkgs-oe-riscv/combine-appliance/noarch/
enabled=1
gpgcheck=0

EOF

# systemd starts serial consoles on /dev/ttyS0 and /dev/hvc0.  The
# only problem is they are the same serial console.  Mask one.
systemctl mask serial-getty@hvc0.service

# setup login message
cat << EOF | tee /etc/issue /etc/issue.net
Welcome to the openEuler/RISC-V disk image

Build date: $(date --utc)

Kernel \r on an \m (\l)

The root password is ‘riscv’.

To install new packages use 'dnf install ...'

To upgrade disk image use 'dnf upgrade --best'

If DNS isn’t working, try editing ‘/etc/yum.repos.d/openEuler-riscv.repo’.

EOF
%end

# EOF
